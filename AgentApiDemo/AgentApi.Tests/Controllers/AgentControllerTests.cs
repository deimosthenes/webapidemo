﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using AgentApi.Controllers;
using AgentApi.Models.Agent;
using AgentCore.Repositories;
using AgentCore.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AgentApi.Tests.Controllers
{
    [TestClass]
    public class AgentControllerTests
    {
        private Mock<IAgentRepository> _mockRepository;
        private AgentController _target;

        [TestInitialize]
        public void TestInitialize()
        {
            _mockRepository = new Mock<IAgentRepository>(MockBehavior.Strict);
            _target = new AgentController(_mockRepository.Object);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _mockRepository.VerifyAll();
        }

        [TestMethod]
        public void Create_ValidatesModelState()
        {
            _target.ModelState.AddModelError("some error", new Exception());

            var model = new CreateRequest();
            var result = _target.Create(model);

            Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));
        }

        [TestMethod]
        public void Create_SavesNewAgent()
        {
            const int expectedId = 123;
            const string firstName = "firstname";
            const string lastName = "lastname";
            const string emailAddress = "emailaddress";
            _mockRepository.Setup(m => m.CreateNewAgent(firstName, lastName, emailAddress)).Returns(expectedId);

            var model = new CreateRequest {FirstName = firstName, LastName = lastName, EmailAddress = emailAddress};
            var result = _target.Create(model);

            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<int>));
            var resultId = (result as OkNegotiatedContentResult<int>).Content;
            Assert.AreEqual(expectedId, resultId);
        }

        [TestMethod]
        public void Create_HandlesException()
        {
            const string firstName = "firstname";
            const string lastName = "lastname";
            const string emailAddress = "emailaddress";
            var exception = new Exception();
            _mockRepository.Setup(m => m.CreateNewAgent(firstName, lastName, emailAddress)).Throws(exception);
            var model = new CreateRequest { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress };

            var result = _target.Create(model);

            Assert.IsInstanceOfType(result, typeof(ExceptionResult));
            var thrownException = (result as ExceptionResult).Exception;
            Assert.AreEqual(exception, thrownException);
        }

        [TestMethod]
        public void Retrieve_ReturnsDetails()
        {
            const int agentId = 123;
            var expectedAgent = new Agent {Id = agentId};
            _mockRepository.Setup(m => m.GetAgent(agentId)).Returns(expectedAgent);

            var result = _target.Retrieve(agentId);

            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<Agent>));
            var resultAgent = (result as OkNegotiatedContentResult<Agent>).Content;
            Assert.AreEqual(expectedAgent, resultAgent);
        }

        [TestMethod]
        public void Retrieve_AgentNotFound()
        {
            const int agentId = 123;
            _mockRepository.Setup(m => m.GetAgent(agentId)).Returns((Agent)null);

            var result = _target.Retrieve(agentId);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void Retrieve_HandlesException()
        {
            const int agentId = 123;
            var exception = new Exception();
            _mockRepository.Setup(m => m.GetAgent(agentId)).Throws(exception);

            var result = _target.Retrieve(agentId);

            Assert.IsInstanceOfType(result, typeof(ExceptionResult));
            var thrownException = (result as ExceptionResult).Exception;
            Assert.AreEqual(exception, thrownException);
        }

        [TestMethod]
        public void Search_NoSearchParameters()
        {
            const int defaultPageSize = 20;
            const int defaultPage = 0;
            var searchRequest = new SearchRequest();
            var searchResults = new List<Agent>
            {
                new Agent {Id = 0},
                new Agent {Id = 1}
            }.AsQueryable();
            _mockRepository.Setup(m => m.FindAgents(null, null, defaultPageSize, defaultPage)).Returns(searchResults);

            var result = _target.Search(searchRequest);

            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IQueryable<Agent>>));
            var resultAgents = (result as OkNegotiatedContentResult<IQueryable<Agent>>).Content;
            Assert.AreEqual(searchResults, resultAgents);
        }

        [TestMethod]
        public void Search_AllSearchParameters()
        {
            const int pageSize = 2;
            const int pageNumber = 3;
            const string agentName = "test";
            const bool hasAgency = false;
            var searchRequest = new SearchRequest
            {
                AgentName = agentName,
                HasAgency = hasAgency,
                MaxAgentsPerPage = pageSize,
                PageNumber = pageNumber
            };
            var searchResults = new List<Agent>
            {
                new Agent {Id = 0},
                new Agent {Id = 1}
            }.AsQueryable();
            _mockRepository.Setup(m => m.FindAgents(agentName, hasAgency, pageSize, pageNumber)).Returns(searchResults);

            var result = _target.Search(searchRequest);

            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IQueryable<Agent>>));
            var resultAgents = (result as OkNegotiatedContentResult<IQueryable<Agent>>).Content;
            Assert.AreEqual(searchResults, resultAgents);
        }

        [TestMethod]
        public void Search_ValidatesModelState()
        {
            _target.ModelState.AddModelError("some error", new Exception());

            var model = new SearchRequest();
            var result = _target.Search(model);

            Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));
        }

        [TestMethod]
        public void Search_HandlesException()
        {
            const int defaultPageSize = 20;
            const int defaultPage = 0;
            var searchRequest = new SearchRequest();
            var exception = new Exception();
            _mockRepository.Setup(m => m.FindAgents(null, null, defaultPageSize, defaultPage)).Throws(exception);

            var result = _target.Search(searchRequest);

            Assert.IsInstanceOfType(result, typeof(ExceptionResult));
            var thrownException = (result as ExceptionResult).Exception;
            Assert.AreEqual(exception, thrownException);
        }


        [TestMethod]
        public void Update_ValidatesModelState()
        {
            _target.ModelState.AddModelError("some error", new Exception());

            var model = new UpdateRequest();
            var result = _target.Update(0, model);

            Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));
        }

        [TestMethod]
        public void Update_Succeeds()
        {
            const int agentId = 123;
            const string firstName = "firstname";
            const string lastName = "lastname";
            const string emailAddress = "emailaddress";
            const int agencyId = 456;
            _mockRepository.Setup(m => m.UpdateAgent(agentId, firstName, lastName, emailAddress, agencyId))
                .Returns(UpdateResponseCode.Success);

            var model = new UpdateRequest { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress, AgencyId = agencyId};
            var result = _target.Update(agentId, model);

            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

        [TestMethod]
        public void Update_AgentNotFound()
        {
            const int agentId = 123;
            const string firstName = "firstname";
            const string lastName = "lastname";
            const string emailAddress = "emailaddress";
            const int agencyId = 456;
            _mockRepository.Setup(m => m.UpdateAgent(agentId, firstName, lastName, emailAddress, agencyId))
                .Returns(UpdateResponseCode.AgentNotFound);

            var model = new UpdateRequest { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress, AgencyId = agencyId };
            var result = _target.Update(agentId, model);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void Update_AgencyNotFound()
        {
            const int agentId = 123;
            const string firstName = "firstname";
            const string lastName = "lastname";
            const string emailAddress = "emailaddress";
            const int agencyId = 456;
            _mockRepository.Setup(m => m.UpdateAgent(agentId, firstName, lastName, emailAddress, agencyId))
                .Returns(UpdateResponseCode.AgencyNotFound);

            var model = new UpdateRequest { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress, AgencyId = agencyId };
            var result = _target.Update(agentId, model);

            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void Update_HandlesException()
        {
            const int agentId = 123;
            const string firstName = "firstname";
            const string lastName = "lastname";
            const string emailAddress = "emailaddress";
            const int agencyId = 456;
            var exception = new Exception();
            _mockRepository.Setup(m => m.UpdateAgent(agentId, firstName, lastName, emailAddress, agencyId))
                .Throws(exception);

            var model = new UpdateRequest { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress, AgencyId = agencyId };
            var result = _target.Update(agentId, model);

            Assert.IsInstanceOfType(result, typeof(ExceptionResult));
            var thrownException = (result as ExceptionResult).Exception;
            Assert.AreEqual(exception, thrownException);
        }


    }
}
