﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentCore.Repositories;
using AgentCore.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AgentCore.Tests.Repositories
{
    [TestClass]
    public class InMemoryAgentRepositoryTests
    {

        private InMemoryAgentRepository _target;

        [TestInitialize]
        public void TestInitialize()
        {
            _target = new InMemoryAgentRepository();
        }

        [TestMethod]
        public void CreateAndGet_SingleRecord_Succeeds()
        {
            const string firstName = "firstName";
            const string lastName = "lastName";
            const string email = "email";

            var newAgentId = _target.CreateNewAgent(firstName, lastName, email);

            var retrieveResult = _target.GetAgent(newAgentId);

            Assert.IsNotNull(retrieveResult);
            Assert.AreEqual(firstName, retrieveResult.FirstName);
            Assert.AreEqual(lastName, retrieveResult.LastName);
            Assert.AreEqual(email, retrieveResult.Email);
            Assert.IsNull(retrieveResult.Agency);
            Assert.AreEqual(firstName + " " + lastName, retrieveResult.FullName);
        }

        // Validate that the RIGHT record is returned when there are several in the store
        [TestMethod]
        public void CreateAndGet_MultipleRecords_Succeeds()
        {
            const string firstName1 = "firstName1";
            const string firstName2 = "firstName2";
            const string lastName = "lastName";
            const string email = "email";

            var newAgentId1 = _target.CreateNewAgent(firstName1, lastName, email);
            var newAgentId2 = _target.CreateNewAgent(firstName2, lastName, email);

            var retrieveResult1 = _target.GetAgent(newAgentId1);
            var retrieveResult2 = _target.GetAgent(newAgentId2);

            Assert.IsNotNull(retrieveResult1);
            Assert.IsNotNull(retrieveResult2);

            Assert.AreEqual(newAgentId1, retrieveResult1.Id);
            Assert.AreEqual(newAgentId2, retrieveResult2.Id);
            Assert.AreEqual(firstName1, retrieveResult1.FirstName);
            Assert.AreEqual(firstName2, retrieveResult2.FirstName);
        }

        [TestMethod]
        public void GetAgent_InvalidId_ReturnsNull()
        {
            const int agentId = 123;
            // Agent store is empty
            var retrieveResult = _target.GetAgent(agentId);

            Assert.IsNull(retrieveResult);
        }

        [TestMethod]
        public void UpdateAgent_InvalidId_ReturnsAgentNotFound()
        {
            const int agentId = 123;
            const string firstName = "firstName";
            const string lastName = "lastName";
            const string email = "email";
            // Agent store is empty
            var updateResult = _target.UpdateAgent(agentId, firstName, lastName, email, null);

            Assert.AreEqual(UpdateResponseCode.AgentNotFound, updateResult);
        }

        [TestMethod]
        public void UpdateAgent_InvalidAgencyId_ReturnsAgencyNotFound()
        {
            const int agencyId = 123;
            const string firstName = "firstName";
            const string lastName = "lastName";
            const string email = "email";

            var newAgentId = _target.CreateNewAgent(firstName, lastName, email);

            var updateResult = _target.UpdateAgent(newAgentId, firstName, lastName, email, agencyId);

            Assert.AreEqual(UpdateResponseCode.AgencyNotFound, updateResult);
        }

        [TestMethod]
        public void UpdateAgent_NoAgency_ReturnsSuccess()
        {
            const string oldFirstName = "firstName";
            const string oldLastName = "lastName";
            const string oldEmail = "email";

            const string newFirstName = "updatedFirstName";
            const string newLastName = "updatedLastName";
            const string newEmail = "updatedEmail";

            var newAgentId = _target.CreateNewAgent(oldFirstName, oldLastName, oldEmail);

            var updateResult = _target.UpdateAgent(newAgentId, newFirstName, newLastName, newEmail, null);

            var retrieveResult = _target.GetAgent(newAgentId);

            Assert.AreEqual(UpdateResponseCode.Success, updateResult);
            Assert.IsNotNull(retrieveResult);
            Assert.AreEqual(newFirstName, retrieveResult.FirstName);
            Assert.AreEqual(newLastName, retrieveResult.LastName);
            Assert.AreEqual(newEmail, retrieveResult.Email);
            Assert.IsNull(retrieveResult.Agency);
            Assert.AreEqual(newFirstName + " " + newLastName, retrieveResult.FullName);
        }

        // TODO: Currently this relies on there being hardcoded reference data for Agencies. If the project were extended to provide CRUD access to them, some tests here should be rewritten
        [TestMethod]
        public void UpdateAgent_HasAgency_ReturnsSuccess()
        {
            const string oldFirstName = "firstName";
            const string oldLastName = "lastName";
            const string oldEmail = "email";

            const string newFirstName = "updatedFirstName";
            const string newLastName = "updatedLastName";
            const string newEmail = "updatedEmail";
            const int agencyId = 0;

            var newAgentId = _target.CreateNewAgent(oldFirstName, oldLastName, oldEmail);

            var updateResult = _target.UpdateAgent(newAgentId, newFirstName, newLastName, newEmail, agencyId);

            var retrieveResult = _target.GetAgent(newAgentId);

            Assert.AreEqual(UpdateResponseCode.Success, updateResult);
            Assert.IsNotNull(retrieveResult);
            Assert.AreEqual(newFirstName, retrieveResult.FirstName);
            Assert.AreEqual(newLastName, retrieveResult.LastName);
            Assert.AreEqual(newFirstName + " " + newLastName, retrieveResult.FullName);
            Assert.AreEqual(newEmail, retrieveResult.Email);

            Assert.IsNotNull(retrieveResult.Agency);
            Assert.AreEqual(agencyId, retrieveResult.Agency.Id);
            Assert.IsNotNull(retrieveResult.Agency.Name);
            Assert.IsNotNull(retrieveResult.Agency.Address);
            Assert.IsNotNull(retrieveResult.Agency.Website);
        }

        [TestMethod]
        public void FindAgents_NoFilter_ReturnsAllAgentsAlphabetically()
        {
            const int pageSize = 20;
            const int pageNumber = 0;
            // newAgent1 will have an earlier ID than newAgent2 and will be earlier in the list - make sure the results are alphabetically sorted on name
            const string firstName1 = "firstNamezzz";
            const string firstName2 = "firstNameaaa";
            const string lastName = "lastName";
            const string email = "email";

            var newAgentId1 = _target.CreateNewAgent(firstName1, lastName, email);
            var newAgentId2 = _target.CreateNewAgent(firstName2, lastName, email);

            var findResult = _target.FindAgents(null, null, pageSize, pageNumber).ToList();

            Assert.IsNotNull(findResult);
            Assert.AreEqual(2, findResult.Count());
            Assert.IsTrue(findResult[0].Id == newAgentId2);
            Assert.IsTrue(findResult[1].Id == newAgentId1);
        }

        [TestMethod]
        public void FindAgents_FiltersOnAgentName_ReturnsMatchingAgents()
        {
            const int pageSize = 20;
            const int pageNumber = 0;
            const string firstName1 = "firstName";
            const string firstName2 = "firstName2";
            const string lastName1 = "lastName";
            const string lastName2 = "lastName2";
            const string email = "email";

            const string agentName = "rstName lastName";

            var newAgentId1 = _target.CreateNewAgent(firstName1, lastName1, email); // Matches
            var newAgentId2 = _target.CreateNewAgent(firstName2, lastName1, email); // Doesn't match
            var newAgentId3 = _target.CreateNewAgent(firstName1, lastName2, email); // Matches

            var findResult = _target.FindAgents(agentName, null, pageSize, pageNumber);

            Assert.IsNotNull(findResult);
            Assert.AreEqual(2, findResult.Count());
            Assert.IsTrue(findResult.Any(x => x.Id == newAgentId1));
            Assert.IsTrue(findResult.Any(x => x.Id == newAgentId3));
        }

        [TestMethod]
        public void FindAgents_FiltersOnAgentNameAndAgency_ReturnsMatchingAgents()
        {
            const int pageSize = 20;
            const int pageNumber = 0;
            const string firstName1 = "firstName";
            const string firstName2 = "firstName2";
            const string lastName1 = "lastName";
            const string lastName2 = "lastName2";
            const string email = "email";
            const int agencyId = 0;

            const string agentName = "rstName lastName";

            var newAgentId1 = _target.CreateNewAgent(firstName1, lastName1, email); // Matches name
            var newAgentId2 = _target.CreateNewAgent(firstName2, lastName1, email); // Doesn't match name
            var newAgentId3 = _target.CreateNewAgent(firstName1, lastName2, email); // Matches name

            var updateResult = _target.UpdateAgent(newAgentId1, firstName1, lastName1, email, agencyId);

            var findResult1 = _target.FindAgents(agentName, true, pageSize, pageNumber); // Matches on newAgentId1
            var findResult2 = _target.FindAgents(agentName, false, pageSize, pageNumber); // Matches on newAgentId3

            Assert.IsNotNull(findResult1);
            Assert.AreEqual(1, findResult1.Count());
            Assert.IsTrue(findResult1.Any(x => x.Id == newAgentId1));

            Assert.IsNotNull(findResult2);
            Assert.AreEqual(1, findResult2.Count());
            Assert.IsTrue(findResult2.Any(x => x.Id == newAgentId3));
        }

        // Tests that paging works, sorting is stable and results are sorted alphabetically between pages
        [TestMethod]
        public void FindAgents_PagingTest()
        {
            const string email = "email";
            const int pageSize = 2;

            var newAgentId1 = _target.CreateNewAgent("aaa", "bbb", email); // 2nd
            var newAgentId2 = _target.CreateNewAgent("bbb", "bbb", email); // 4rd
            var newAgentId3 = _target.CreateNewAgent("aaa", "aaa", email); // 1st
            var newAgentId4 = _target.CreateNewAgent("bbb", "aaa", email); // 3rd
            var newAgentId5 = _target.CreateNewAgent("bbb", "bbb", email); // 5st

            var findResultPage0 = _target.FindAgents(null, null, pageSize, 0).ToList();
            var findResultPage1 = _target.FindAgents(null, null, pageSize, 1).ToList();
            var findResultPage2 = _target.FindAgents(null, null, pageSize, 2).ToList();

            Assert.AreEqual(2, findResultPage0.Count());
            Assert.AreEqual(newAgentId3, findResultPage0[0].Id);
            Assert.AreEqual(newAgentId1, findResultPage0[1].Id);

            Assert.AreEqual(2, findResultPage1.Count());
            Assert.AreEqual(newAgentId4, findResultPage1[0].Id);
            Assert.AreEqual(newAgentId2, findResultPage1[1].Id);

            Assert.AreEqual(1, findResultPage2.Count());
            Assert.AreEqual(newAgentId5, findResultPage2[0].Id);
        }
    }
}
