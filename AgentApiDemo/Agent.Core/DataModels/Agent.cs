﻿namespace AgentCore.DataModels
{
    public class Agent
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? AgencyId { get; set; }

        // Currently used for searching. I've assumed anyone searching for full name would be including a single space character between first and last names.
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
