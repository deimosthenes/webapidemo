﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgentCore.DataModels;
using AgentCore.ViewModels;

namespace AgentCore.Repositories
{
    
    public class InMemoryAgentRepository : IAgentRepository
    {
        // Represents the contents of this repository. Only works because this repository instance persists for the life of the unity container.
        private readonly List<DataModels.Agent> _agents;
        private readonly List<DataModels.Agency> _agencies;

        private int _nextAgentId = 0;

        public InMemoryAgentRepository()
        {
            _agents = new List<DataModels.Agent>();

            // Currently there are no methods available to perform CRUD on the Agencies, only to link an agent to existing agencies. I've mocked up some fake agency data to make this testable.
            _agencies = new List<DataModels.Agency>
            {
                new DataModels.Agency
                {
                    Id = 0,
                    Name = "Test Agency 0",
                    Address = "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France",
                    Website = "http://testagency0.local"
                },
                new DataModels.Agency
                {
                    Id = 1,
                    Name = "Test Agency 1",
                    Address = "Red Square, Moscow, Russia, 109012",
                    Website = "http://testagency1.local"
                },
                new DataModels.Agency
                {
                    Id = 2,
                    Name = "Test Agency 2",
                    Address = "Place d'Armes, 78000 Versailles, France",
                    Website = "http://testagency2.local"
                },
                new DataModels.Agency
                {
                    Id = 3,
                    Name = "Test Agency 3"
                },
                new DataModels.Agency
                {
                    Id = 4,
                    Name = "Test Agency 4"
                },
                new DataModels.Agency
                {
                    Id = 5,
                    Name = "Test Agency 5"
                }
            };
        }
        
        public ViewModels.Agent GetAgent(int id)
        {
            return BuildAgentView(_agents.AsQueryable().Where(ag => ag.Id == id)).SingleOrDefault();
        }

        public IQueryable<ViewModels.Agent> FindAgents(string agentName, bool? hasAgency, int maxAgentsPerPage, int pageNumber)
        {
            IQueryable<DataModels.Agent> query = _agents.AsQueryable();
            // Conditional filter for hasAgency
            if (hasAgency.HasValue)
            {
                query = query.Where(x => x.AgencyId.HasValue == hasAgency);
            }
            // Conditional filter for agentName substring matching
            if (!String.IsNullOrWhiteSpace(agentName))
            {
                query = query.Where(x => x.FullName.Contains(agentName));
            }

            // Sorting alphabetically on full name, then a secondary sort on ID to ensure stable order
            query = query.OrderBy(x => x.FullName).ThenBy(x => x.Id);

            // Basic paging mechanism
            query = query.Skip(maxAgentsPerPage * pageNumber).Take(maxAgentsPerPage);

            return BuildAgentView(query);
        }

        // Creates a new agent, adding it to the in-memory collection and providing it with an ID.
        // If this was anything other than a simple single-threaded demo, there would need to be locking to ensure atomicity and prevent duplicate IDs
        // Aditionally, you'd want to do the same data validity checks that the API does (FirstName and LastName mandatory, etc.) here as well in case of code reuse
        public int CreateNewAgent(string firstName, string lastName, string emailAddress)
        {
            var agent = new DataModels.Agent
            {
                Id = _nextAgentId,
                FirstName = firstName,
                LastName = lastName,
                Email = emailAddress
            };
            _agents.Add(agent);
            _nextAgentId++;
            return agent.Id;
        }

        public UpdateResponseCode UpdateAgent(int agentId, string firstName, string lastName, string emailAddress, int? agencyId)
        {
            // Validate that the agent exists
            var agentToUpdate = _agents.SingleOrDefault(a => a.Id == agentId);
            if (agentToUpdate == null)
            {
                return UpdateResponseCode.AgentNotFound;
            }
            // If an agency ID is specified, it needs to exist within the store
            if (agencyId.HasValue && !_agencies.Any(a => a.Id == agencyId.Value))
            {
                return UpdateResponseCode.AgencyNotFound;
            }
            agentToUpdate.FirstName = firstName;
            agentToUpdate.LastName = lastName;
            agentToUpdate.Email = emailAddress;
            agentToUpdate.AgencyId = agencyId;
            return UpdateResponseCode.Success;
        }


        // Given a collection of agent "rows" from our fake database, does a join to pull in agency details then maps the result to a view model suitable for the API to return
        private IQueryable<ViewModels.Agent> BuildAgentView(IQueryable<DataModels.Agent> agents)
        {
            return agents.GroupJoin(_agencies, x => x.AgencyId, y => y.Id,
                    (agent, agencies) => MapToView(agent, agencies.SingleOrDefault()));
        }

        // Mapper method to abstract away database structure and implementation from end-user / consuming service view.
        // In a real system you'd probably refactor this away to somewhere more reusable.
        private ViewModels.Agent MapToView(DataModels.Agent agent, DataModels.Agency agency)
        {
            var agencyResult = agency == null
                ? null
                : new ViewModels.Agency
                {
                    Id = agency.Id,
                    Name = agency.Name,
                    Address = agency.Address,
                    Website = agency.Website
                };

            return new ViewModels.Agent
            {
                Id = agent.Id,
                FirstName = agent.FirstName,
                LastName = agent.LastName,
                FullName = agent.FullName,
                Email = agent.Email,
                Agency = agencyResult
            };
        }

    }
     
}
