﻿using System.Collections.Generic;
using System.Linq;
using AgentCore.ViewModels;

namespace AgentCore.Repositories
{
    public class DummyAgentRepository : IAgentRepository
    {
        public Agent GetAgent(int id)
        {
            return new Agent {Id = id, Email = "email@email.net", FirstName = "Test", LastName = "Agent", FullName = "Test Agent"};
        }

        public IQueryable<Agent> FindAgents(string agentName, bool? hasAgency, int maxAgentsPerPage, int pageNumber)
        {
            return new List<Agent>{GetAgent(1), GetAgent(2)}.AsQueryable();
        }

        public int CreateNewAgent(string firstName, string lastName, string emailAddress)
        {
            return 1;
        }

        public UpdateResponseCode UpdateAgent(int agentId, string firstName, string lastName, string emailAddress, int? agencyId)
        {
            return UpdateResponseCode.Success;
        }
    }
}
