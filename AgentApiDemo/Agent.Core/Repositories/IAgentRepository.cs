﻿using System.Linq;
using AgentCore.ViewModels;

namespace AgentCore.Repositories
{
    public interface IAgentRepository
    {
        Agent GetAgent(int id);

        IQueryable<Agent> FindAgents(string agentName, bool? hasAgency, int maxAgentsPerPage, int pageNumber);

        int CreateNewAgent(string firstName, string lastName, string emailAddress);

        UpdateResponseCode UpdateAgent(int agentId, string firstName, string lastName, string emailAddress, int? agencyId);
    }
}
