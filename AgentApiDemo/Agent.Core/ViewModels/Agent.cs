﻿namespace AgentCore.ViewModels
{
    public class Agent
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Agency Agency { get; set; }

        // Ideally this should be a calculated property using FirstName and LastName
        // For the JSON serializer this works fine. For the XML serializer it was missing from the response
        // so I've changed the calling code to set it directly.
        public string FullName { get; set; }
    }
}
