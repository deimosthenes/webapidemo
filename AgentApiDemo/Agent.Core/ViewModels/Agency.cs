﻿namespace AgentCore.ViewModels
{
    public class Agency
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
    }
}
