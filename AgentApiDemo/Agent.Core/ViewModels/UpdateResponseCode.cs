﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCore.ViewModels
{
    public enum UpdateResponseCode
    {
        Success,
        AgentNotFound,
        AgencyNotFound
    }
}
