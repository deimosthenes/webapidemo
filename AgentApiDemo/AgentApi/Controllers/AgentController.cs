﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using AgentApi.Models.Agent;
using AgentCore.Repositories;
using AgentCore.ViewModels;

namespace AgentApi.Controllers
{
    public class AgentController : ApiController
    {
        private const int MaxPageSize = 20;
        private readonly IAgentRepository _agentRepository;

        public AgentController(IAgentRepository agentRepository)
        {
            _agentRepository = agentRepository;
        }

        /// <summary>
        /// Retrieves details about a single agent
        /// </summary>
        /// <param name="id">The unique ID of the agent.</param>
        /// <returns>Details on the current state of the agent.</returns>
        /// <response code="200">The agent details were retrieved.</response>
        /// <remarks>Error HTTP Codes: 404 if agent could not be found, 500 for an unexpected error</remarks>
        [Route("Agent/{id}")]
        [AcceptVerbs("GET")]
        [ResponseType(typeof(Agent))]
        public IHttpActionResult Retrieve(int id)
        {
            Trace.TraceInformation("Request received to retrieve agent with ID " + id);

            try
            {
                var result = _agentRepository.GetAgent(id);

                if (result == null)
                {
                    Trace.TraceWarning("Could not find agent with ID " + id);
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("Could not retrieve the agent: {0}", ex));

                // TODO: Hide internal exception details from API users if using this as anything but a demo
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Searches for agents matching the specified criteria
        /// </summary>
        /// <param name="request">The optional parameters of the search</param>
        /// <returns>The set of matching agents, alphabetically sorted on agent name.</returns>
        /// <response code="200">The search completed successfully</response>
        /// <remarks>Error HTTP Codes: 400 for a validation error, 500 for an unexpected error</remarks>
        /// <remarks>Tying into OData query syntax as a quick and easy way to get paging. The OData parameters $skip and $top can be used to page through results.
        /// The page size is restricted to 20 server-side.</remarks>
        [Route("Agent/Search")]
        [AcceptVerbs("GET")]
        [ResponseType(typeof(IQueryable<Agent>))]
        public IHttpActionResult Search([FromUri] SearchRequest request)
        {
            var searchName = request == null ? null : request.AgentName;
            var searchHasAgency = request == null ? null : request.HasAgency;
            // Default values for paging if they're not set up in the request - 20 agents per page, starting at page 0
            var maxAgentsPerPage = request == null || !request.MaxAgentsPerPage.HasValue ? MaxPageSize : request.MaxAgentsPerPage.Value;
            var pageNumber = request == null || !request.PageNumber.HasValue ? 0 : request.PageNumber.Value;
            // ReSharper disable once RedundantStringFormatCall - string.format syntax is broken for Trace calls in Azure diagnostics
            Trace.TraceInformation(string.Format("Request received to search for agents. Name = '{0}' and HasAgency = '{1}'", searchName, searchHasAgency));
            if (!ModelState.IsValid)
            {
                Trace.TraceError("Search request model was invalid. Request: " + request);
                return BadRequest(ModelState);
            }
            try
            {
                var result = _agentRepository.FindAgents(searchName, searchHasAgency, maxAgentsPerPage, pageNumber);

                return Ok(result);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("Error while searching for agents: {0}", ex));

                // TODO: Hide internal exception details from API users if using this as anything but a demo
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Creates a new agent
        /// </summary>
        /// <param name="request">The details of the new agent to be created.</param>
        /// <returns>The ID of the newly created agent</returns>
        /// <response code="200">The agent was successfully created</response>
        /// <remarks>Error HTTP Codes: 400 for a validation error, 500 for an unexpected error</remarks>
        [Route("Agent")]
        [AcceptVerbs("PUT")]
        [ResponseType(typeof(int))]
        public IHttpActionResult Create(CreateRequest request)
        {
            Trace.TraceInformation("Request received to create a new Agent: " + request);
            if (!ModelState.IsValid)
            {
                Trace.TraceError("Agent model was invalid: " + request);
                return BadRequest(ModelState);
            }
            try
            {
                var result = _agentRepository.CreateNewAgent(request.FirstName, request.LastName, request.EmailAddress);

                return Ok(result);
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("Could not create new contact: {0}", ex));

                // TODO: Hide internal exception details from API users if using this as anything but a demo
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Updates an agent
        /// </summary>
        /// <param name="id">The unique ID of the agent.</param>
        /// <param name="request">The updated values of the agent fields</param>
        /// <returns>The result of the attempted update</returns>
        /// <response code="200">The agent was updated</response>
        /// <remarks>Error HTTP Codes: 400 for a validation error (including invalid Agency ID), 404 if agent could not be found, 500 for an unexpected error</remarks>
        [Route("Agent/{id}")]
        [AcceptVerbs("POST")]
        public IHttpActionResult Update(int id, UpdateRequest request)
        {
            // ReSharper disable once RedundantStringFormatCall
            Trace.TraceInformation(String.Format("Request received to update agent with ID {0} and model {1}", id, request));
            if (!ModelState.IsValid)
            {
                Trace.TraceError(String.Format("Update model was invalid. ID: {0}, request: {1}", id, request));
                return BadRequest(ModelState);
            }
            try
            {
                var resultCode = _agentRepository.UpdateAgent(id, request.FirstName, request.LastName, request.EmailAddress, request.AgencyId);

                switch (resultCode)
                {
                    case UpdateResponseCode.Success:
                        return Ok();
                    case UpdateResponseCode.AgentNotFound:
                        Trace.TraceWarning("Could not find agent with ID " + id);
                        return NotFound();
                    case UpdateResponseCode.AgencyNotFound:
                        Trace.TraceWarning("Could not find agency with ID " + request.AgencyId);
                        return BadRequest("An Agency with the specified ID " + request.AgencyId + " could not be found.");
                    default:
                        throw new Exception("Unexpected result from agent repository during update");
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(string.Format("Could not retrieve the agent: {0}", ex));

                // TODO: Hide internal exception details from API users if using this as anything but a demo
                return InternalServerError(ex);
            }
        }

    }
}