﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Security;
using System.Web.Http.OData.Query;

namespace AgentApi.Models.Agent
{
    [DataContract(Name = "AgentSearchRequest")]
    public class SearchRequest
    {
        /// <summary>
        /// Restricts the search to agents with a matching name. Will match to first name or last name or both.
        /// </summary>
        [DataMember(Name = "AgentName")]
        [StringLength(100, ErrorMessage = "The {0} must be at most {1} characters long.")]
        [Display(Name = "Agent name")]
        public string AgentName { get; set; }

        // Specification is not explicit, so I've assumed HasAgency = false means filtering to agents without an agency, rather than the filter being ignored
        /// <summary>
        /// Restricts the search to only agents that do or do not have an agency
        /// </summary>
        [DataMember(Name = "HasAgency")]
        [Display(Name = "Has agency")]
        public bool? HasAgency { get; set; }

        /// <summary>
        /// Restricts the number of results returned by the search. 
        /// </summary>
        [DataMember(Name = "MaxAgentsPerPage")]
        [Display(Name = "Max agents per page")]
        [Range(typeof(int), "1", "20")]
        public int? MaxAgentsPerPage { get; set; }

        /// <summary>
        /// Used with MaxAgentsPerPage to support pagination. Results start at page 0.
        /// </summary>
        [DataMember(Name = "PageNumber")]
        [Display(Name = "Page number")]
        public int? PageNumber { get; set; }
    }
}