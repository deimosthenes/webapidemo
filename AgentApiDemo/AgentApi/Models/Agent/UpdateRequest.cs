﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AgentApi.Models.Agent
{
    public class UpdateRequest
    {
        /// <summary>
        /// The new first name for the agent
        /// </summary>
        [DataMember(Name = "FirstName")]
        [Display(Name = "First name")]
        [StringLength(50)]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// The new last name for the agent
        /// </summary>
        [DataMember(Name = "LastName")]
        [Display(Name = "Last name")]
        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// The new email address for the agent
        /// </summary>
        [DataMember(Name = "EmailAddress")]
        [Display(Name = "Email address")]
        [StringLength(250)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The new Agency ID for the agent. Must match an agency already in the system.
        /// </summary>
        [DataMember(Name = "AgencyId")]
        [Display(Name = "Agency ID")]
        public int? AgencyId { get; set; }
    }
}