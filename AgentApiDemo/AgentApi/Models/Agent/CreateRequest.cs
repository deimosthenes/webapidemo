﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AgentApi.Models.Agent
{
    public class CreateRequest
    {
        /// <summary>
        /// The first name of the new agent
        /// </summary>
        [DataMember(Name = "FirstName")]
        [Display(Name = "First name")]
        [StringLength(50)]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the new agent
        /// </summary>
        [DataMember(Name = "LastName")]
        [Display(Name = "Last name")]
        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// The email address of the new agent
        /// </summary>
        [DataMember(Name = "EmailAddress")]
        [Display(Name = "Email address")]
        [StringLength(250)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string EmailAddress { get; set; }
    }
}